﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class Summary : IGpsMessage {

        public DateTime Time { get; set; }
        public Status Status { get; set; }
        public Position Position { get; set; }
        public double GroundSpeed { get; set; }
        public double Heading { get; set; }
        public MagneticVariation MagneticVariation { get; set; }

    }
}
