﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {

    public enum FixType {
        NotAvailable = 0,
        TwoDimensional = 2,
        ThreeDimensional = 3,
    }

}
