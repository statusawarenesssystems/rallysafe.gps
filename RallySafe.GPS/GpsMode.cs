﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public enum GpsMode {
        Unknown,
        Autonomous,
        Differential,
        DeadReckoning,
        ManualInput,
        Simulator
    }
}
