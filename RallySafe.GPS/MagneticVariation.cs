﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class MagneticVariation {
        public decimal Degrees { get; set; }
        public Direction Direction { get; set; }
    }
}
