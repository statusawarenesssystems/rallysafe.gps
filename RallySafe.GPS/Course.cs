﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class Course : IGpsMessage {

        /// <summary>
        /// True course over ground (COG) in degrees (000 to 359).
        /// </summary>
        public decimal TrueCourseOverGround { get; set; }

        /// <summary>
        /// Magnetic course over ground in degrees (000 to 359).
        /// </summary>
        public decimal MagneticCourseOverGround { get; set; }

        /// <summary>
        /// Speed over ground in knots.
        /// </summary>
        public decimal SpeedInKnots { get; set; }

        /// <summary>
        /// Speed over ground in km/h.
        /// </summary>
        public decimal SpeedInKph { get; set; }

        public GpsMode Mode { get; set; }
    }
}
