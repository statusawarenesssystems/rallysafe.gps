﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {

    /// <summary>
    /// A read-only interface to a GPS device
    /// </summary>
    public interface IGpsInterface {
        /// <summary>
        /// Triggered when the device is disconnected.
        /// </summary>
        event EventHandler Disconnected;

        /// <summary>
        /// Triggered when a valid message is received on the device.
        /// </summary>
        event Action<IGpsMessage> MessageReceived;

        /// <summary>
        /// Triggered when an error is received on the device.
        /// </summary>
        event Action<string> ErrorReceived;

        /// <summary>
        /// Whether the device is connected.
        /// </summary>
        bool Connected { get; }

        /// <summary>
        /// Closes the connection to the device.
        /// </summary>
        void Close();
    }
}
