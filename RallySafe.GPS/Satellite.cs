﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class Satellite {
        /// <summary>
        /// Satellite PRN number
        /// </summary>
        public int PRN { get; set; }

        /// <summary>
        /// Whether or not the satellite was used in the positional fix.
        /// </summary>
        public bool UsedInPositionFix { get; set; }

        /// <summary>
        /// The angle of the satellite degrees, 0-90
        /// </summary>
        public int AngleOfElvation { get; set; }

        /// <summary>
        /// The azimuth of the satellite, in degrees, from true north. 0-359.
        /// </summary>
        public int Azimuth { get; set; }

        /// <summary>
        /// Signal strength, in decibels. 0-99.
        /// </summary>
        public int SignalStrength { get; set; }
    }
}
