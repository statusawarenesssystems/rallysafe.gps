﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class Elevation {

        public decimal Value { get; set; }
        public UnitType UnitType { get; set; }

    }
}
