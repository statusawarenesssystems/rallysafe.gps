﻿using System;
using System.Threading.Tasks;

namespace RallySafe.GPS {

    /// <summary>
    /// A discovery service for GPS devices.
    /// </summary>
    public interface IGpsDiscoverer {

        /// <summary>
        /// Search for any connected devices.
        /// </summary>
        Task<IGpsInterface> Search();

        /// <summary>
        /// Abort any search for connected devices.
        /// </summary>
        void Cancel();

    }
}
