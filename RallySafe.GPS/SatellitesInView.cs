﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class SatellitesInView : IGpsMessage {
        /// <summary>
        /// Total number of messages in this cycle
        /// </summary>
        public int MessageCount { get; set; }

        /// <summary>
        /// Message number
        /// </summary>
        public int MessageNumber { get; set; }

        /// <summary>
        /// Number of satellites in view
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// The satellite number for this message.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Satellite details.
        /// </summary>
        public Satellite Satellite { get; set; }
    }
}
