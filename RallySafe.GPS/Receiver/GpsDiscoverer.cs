﻿using Hardware.ComPort.Experimental;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RallySafe.GPS.Receiver {
    public class GpsDiscoverer : IGpsDiscoverer {

        private int _baudRate;
        private Task<IGpsInterface> _searchTask;

        private AutoResetEvent _devicesChanged;
        private ManagementEventWatcher _monitor;

        public GpsDiscoverer(int baudRate = 9600) {
            _baudRate = baudRate;
            _searchTask = new Task<IGpsInterface>(MonitorPorts);

            _devicesChanged = new AutoResetEvent(false);
            _monitor = new ManagementEventWatcher(new WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent WHERE EventType = 2"));
            _monitor.EventArrived += (sender, e) => _devicesChanged.Set();
        }

        /// <summary>
        /// Start searching for radios.
        /// </summary>
        /// <returns>The task performing the search.</returns>
        public Task<IGpsInterface> Search() {
            return Task.Run(() => MonitorPorts());
        }

        public void Cancel() {
            _monitor.Stop();
        }

        private IGpsInterface MonitorPorts() {
            // Monitor for device changes
            _monitor.Start();

            while (true) {

                IGpsInterface device = QueryPorts();
                if (device != null) {
                    _monitor.Stop();
                    return device;
                }

                // Nothing found yet, wait for device insertion
                NMEA.NotifyInformation("No available port found, waiting for device...");
                _devicesChanged.WaitOne();
            }
        }

        private IGpsInterface QueryPorts() {
            foreach (var portName in SerialPort.GetPortNames().Reverse()) {
                NMEA.NotifyInformation($"Querying port {portName}");
                SerialPort port = new SerialPort(portName, _baudRate, Parity.None, 8, StopBits.One);
                try {
                    port.Open();
                    if (port.IsOpen) {
                        return new GpsInterface(new ComPort(new SerialPortWrapper(port)));
                    }
                } catch (Exception e) {
                    // Not a catastrophic error, most likely not a radio device
                    NMEA.NotifyInformation($"Failed to open port {portName}: {e.Message}");
                }
            }
            return null;
        }
    }
}
