﻿using Hardware.ComPort.Experimental;
using RallySafe.GPS.MessageCatalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace RallySafe.GPS.Receiver {

    public class GpsInterface : IGpsInterface {

        public event EventHandler Disconnected = delegate { };
        public event Action<IGpsMessage> MessageReceived = delegate { };
        public event Action<string> ErrorReceived = delegate { };

        private IPort _port;
        private List<byte> byteBuffer = new List<byte>();

        public GpsInterface(IPort port) {
            if (port == null) {
                throw new ArgumentNullException("port");
            }

            _port = port;
            _port.Closed += OnClosed;
            _port.ErrorReceived += OnErrorReceived;
            //_processor = Task.Run((Action)ProcessPacket);


            var dataReceivedBlock = new ActionBlock<byte[]>(bytes => {
                byteBuffer.AddRange(bytes);

                TryProcessGpsPacket();
            });

            _port.ReadBlock.LinkTo(dataReceivedBlock);
        }

        private void OnErrorReceived(string err) {
            ErrorReceived(err);
        }

        private void TryProcessGpsPacket() {
            byte[] terminator = new byte[2] {0x0D, 0x0A};
            var terminatorPositions = PatternAt(byteBuffer.ToArray(), terminator);
            if (terminatorPositions.Count() > 0) {
                int sourceIndex = 0;

                foreach (var terminatorPosition in terminatorPositions) {
                   

                    // Received the whole message
                    byte[] payload = new byte[terminatorPosition - sourceIndex];

                    Array.Copy(byteBuffer.ToArray(), sourceIndex, payload, 0, payload.Length);
                    sourceIndex = terminatorPosition + 2;

                    var bytesAsString = Encoding.UTF8.GetString(payload);

                    IGpsMessage message = NMEA.ParseMessage(bytesAsString);
                    if (message != null) {                 
                        MessageReceived(message);
                    }
                }
                byteBuffer.RemoveRange(0, sourceIndex);
            }
        }

        public static IEnumerable<int> PatternAt(byte[] source, byte[] pattern) {
            for (int i = 0; i < source.Length; i++) {
                if (source.Skip(i).Take(pattern.Length).SequenceEqual(pattern)) {
                    yield return i;
                }
            }
        }

        private void OnDisconnected(object sender, EventArgs e) {
            Disconnected(this, e);
        }

        public bool Connected {
            get { return true; }
        }

        public void OnClosed() {
            Disconnected(this, null);
        }

        public void Close() {
            _port.Close();
        }
    }

}
