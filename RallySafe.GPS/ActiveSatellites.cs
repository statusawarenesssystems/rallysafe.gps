﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class ActiveSatellites : IGpsMessage {

        public FixMode FixMode { get; set; }

        public FixType FixType { get; set; }

        public IList<Satellite> Satellites { get; set; }

        public decimal DilutionOfPrecision { get; set; }

        public decimal HorizontalDilutionOfPrecision { get; set; }

        public decimal VerticalDilutionOfPrecision { get; set; }
    }
}
