﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class Fix : IGpsMessage {
        /// <summary>
        /// Time of Position Reading
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Position
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// Number of satelites in use (not the number visible).
        /// </summary>
        public int NumbeOfSatellitesInUse { get; set; }

        /// <summary>
        /// Horizontal dilution of precision 
        /// </summary>
        public decimal HorizontalDilutionOfPrecision { get; set; }

        /// <summary>
        /// Antenna altitude above/below mean sea level (geoid)
        /// </summary>
        public Elevation Elevation { get; set; }

        /// <summary>
        /// Geoidal separation (Diff. between WGS-84 earth ellipsoid and mean sea level. '-' = geoid is below WGS-84 ellipsoid) 
        /// </summary>
        public GeoidalSeparation GeoidalSeparation { get; set; }

        /// <summary>
        /// The DGPS difference correction
        /// </summary>
        public DifferenceCorrection DifferenceCorrection { get; set; }

        /// <summary>
        /// GPS Quality
        /// </summary>
        public Quality Quality { get; set; }
    }
}
