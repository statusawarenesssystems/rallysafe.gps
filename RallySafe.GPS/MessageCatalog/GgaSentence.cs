﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS.MessageCatalog {
    internal class GgaSentence {
        public static bool TryParse(string identifier, string payload, out IGpsMessage message) {
            if (identifier.Equals("$GPGGA")) {
                // Strip the checksum
                payload = payload.Substring(0, payload.IndexOf('*'));

                try {
                    var values = payload.Split(',');
                    if (values.Length < 14) {
                        throw new FormatException("Bad GGA sentence structure");
                    }

                    var fix = new Fix();

			        //eg3. $GPGGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
			        //1    = UTC of Position
			        //2    = Latitude
			        //3    = N or S
			        //4    = Longitude
			        //5    = E or W
			        //6    = GPS quality indicator (0=invalid; 1=GPS fix; 2=Diff. GPS fix)
			        //7    = Number of satellites in use [not those in view]
			        //8    = Horizontal dilution of position
			        //9    = Antenna altitude above/below mean sea level (geoid)
			        //10   = Meters  (Antenna height unit)
			        //11   = Geoidal separation (Diff. between WGS-84 earth ellipsoid and
			        //       mean sea level.  -=geoid is below WGS-84 ellipsoid)
			        //12   = Meters  (Units of geoidal separation)
			        //13   = Age in seconds since last update from diff. reference station
			        //14   = Diff. reference station ID#
			        //15   = Checksum

                    if (values[0].Length > 5) {
                        int hour = int.Parse(values[0].Substring(0, 2));
                        int minute = int.Parse(values[0].Substring(2, 2));
                        int second = int.Parse(values[0].Substring(4, 2));

                        var utc = DateTime.UtcNow;
                        fix.Time = new DateTime(utc.Year, utc.Month, utc.Day, hour, minute, second, DateTimeKind.Utc);
                    } else {
                        throw new FormatException("Invalid time");
                    }

                    fix.Position = NMEA.ParsePosition(String.Concat(values[1], ',', values[2], ';', values[3], ',', values[4]));
                    fix.Quality = (Quality)Enum.Parse(typeof(Quality), values[5]);
                    fix.NumbeOfSatellitesInUse = int.Parse(values[6]);
                    fix.HorizontalDilutionOfPrecision = String.IsNullOrEmpty(values[7]) ? 0M : decimal.Parse(values[7]);
                    fix.Elevation = NMEA.ParseElevation(String.Concat(values[8], ',', values[9]));
                    fix.GeoidalSeparation = NMEA.ParseGeoidalSeparation(String.Concat(values[10], ',', values[11]));
                    fix.DifferenceCorrection = new DifferenceCorrection {
                        Age = String.IsNullOrEmpty(values[12]) ? 0M : decimal.Parse(values[12]),
                        StationID = String.IsNullOrEmpty(values[13]) ? 0 : int.Parse(values[13])
                    };

                    message = fix;
                    return true;
                } catch (Exception fe) {
                    NMEA.NotifyError(fe.Message);
                    message = null;
                    return false;
                }
            } else {
                message = null;
                return false;
            }
        }
    }
}
