﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS.MessageCatalog {
    internal class VtgSentence : IGpsMessage {
        // http://www.hemispheregps.com/gpsreference/GPVTG.htm

        public static bool TryParse(string identifier, string payload, out IGpsMessage message) {
            if (identifier.Equals("$GPVTG")) {
                // Strip the checksum
                payload = payload.Substring(0, payload.IndexOf('*'));

                try {
                    var values = payload.Split(',');
                    if (values.Length < 9) {
                        throw new FormatException("Bad VTG sentence structure");
                    }

                    message = new Course {
                        TrueCourseOverGround = String.IsNullOrEmpty(values[0]) ? 0M : decimal.Parse(values[0]),
                        MagneticCourseOverGround = String.IsNullOrEmpty(values[2]) ? 0M : decimal.Parse(values[2]),
                        SpeedInKnots = String.IsNullOrEmpty(values[4]) ? 0M : decimal.Parse(values[4]),
                        SpeedInKph = String.IsNullOrEmpty(values[6]) ? 0M : decimal.Parse(values[6]),
                        Mode = NMEA.ParseGpsMode(values[8])
                    };
                    return true;

                } catch (Exception fe) {
                    NMEA.NotifyError(fe.Message);
                    message = null;
                    return false;
                }
            } else {
                message = null;
                return false;
            }
        }
    }
}
