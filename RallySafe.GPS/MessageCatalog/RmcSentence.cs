﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS.MessageCatalog {
    internal class RmcSentence {
        public static bool TryParse(string identifier, string payload, out IGpsMessage message) {
            if (identifier.Equals("$GPRMC")) {
                // Strip the checksum
                payload = payload.Substring(0, payload.IndexOf('*'));

                try {
                    var values = payload.Split(',');
                    if (values.Length < 12) {
                        throw new FormatException("Bad RMC sentence structure");
                    }

                    var summary = new Summary();

                    //1   220516     Time Stamp
                    //2   A          validity - A-ok, V-invalid
                    //3   5133.82    current Latitude
                    //4   N          North/South
                    //5   00042.24   current Longitude
                    //6   W          East/West
                    //7   173.8      Speed in knots
                    //8   231.8      True course
                    //9   130694     Date Stamp
                    //10  004.2      Variation
                    //11  W          East/West
                    //12  *70        checksum

                    if (values[0].Length > 5) {
                        int hour = int.Parse(values[0].Substring(0, 2));
                        int minute = int.Parse(values[0].Substring(2, 2));
                        int second = int.Parse(values[0].Substring(4, 2));

                        var utc = DateTime.UtcNow;
                        summary.Time = new DateTime(utc.Year, utc.Month, utc.Day, hour, minute, second, DateTimeKind.Utc);
                    } else {
                        throw new FormatException("Invalid time");
                    }
                    summary.Status = (values[1] == "A") ? Status.Valid : Status.Invalid;
                    summary.Position = NMEA.ParsePosition(String.Concat(values[2], ',', values[3], ';', values[4], ',', values[5]));
                    summary.GroundSpeed = String.IsNullOrWhiteSpace(values[6]) ? 0D : double.Parse(values[6]);
                    summary.Heading = String.IsNullOrWhiteSpace(values[7]) ? 0D : double.Parse(values[7]);
                    summary.MagneticVariation = NMEA.ParseMagneticVariation(String.Concat(values[9], ',', values[10]));

                    message = summary;
                    return true;
                } catch (Exception fe) {
                    NMEA.NotifyError(fe.Message);
                    message = null;
                    return false;
                }
            } else {
                message = null;
                return false;
            }
        }
    }
}
