﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS.MessageCatalog {
    internal class GsaSentence {

        public static bool TryParse(string identifier, string payload, out IGpsMessage message) {
            if (identifier.Equals("$GPGSA")) {
                // Strip the checksum
                payload = payload.Substring(0, payload.IndexOf('*'));

                try {
                    var values = payload.Split(',');
                    if (values.Length < 17) {
                        throw new FormatException("Bad GSA sentence structure");
                    }

                    var satellites = new ActiveSatellites();

                    //GPS DOP and active satellites

                    //eg1. $GPGSA,A,3,,,,,,16,18,,22,24,,,3.6,2.1,2.2*3C
                    //eg2. $GPGSA,A,3,19,28,14,18,27,22,31,39,,,,,1.7,1.0,1.3*35

                    //1    = Mode:
                    //       M=Manual, forced to operate in 2D or 3D
                    //       A=Automatic, 3D/2D
                    //2    = Mode:
                    //       1=Fix not available
                    //       2=2D
                    //       3=3D
                    //3-14 = IDs of SVs used in position fix (null for unused fields)
                    //15   = PDOP
                    //16   = HDOP
                    //17   = VDOP

                    satellites.FixMode = NMEA.ParseFixMode(values[0]);
                    satellites.FixType = (FixType)int.Parse(values[1]);
                    satellites.Satellites = new List<Satellite>();
                    for (int i = 2; i < 14; ++i) {
                        if (!String.IsNullOrEmpty(values[i])) {
                            satellites.Satellites.Add(new Satellite {
                                AngleOfElvation = -1,
                                Azimuth = -1,
                                SignalStrength = -1,
                                UsedInPositionFix = false,
                                PRN = int.Parse(values[i]),
                            });
                        }
                    }
                    satellites.DilutionOfPrecision = decimal.Parse(values[14]);
                    satellites.HorizontalDilutionOfPrecision = decimal.Parse(values[15]);
                    satellites.VerticalDilutionOfPrecision = decimal.Parse(values[16]);

                    message = satellites;
                    return true;
                } catch (Exception fe) {
                    NMEA.NotifyError(fe.Message);
                    message = null;
                    return false;
                }
            } else {
                message = null;
                return false;
            }
        }
    }
}
