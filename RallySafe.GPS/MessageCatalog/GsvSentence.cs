﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS.MessageCatalog {
    internal class GsvSentence {
        public static bool TryParse(string identifier, string payload, out IGpsMessage message) {
            if (identifier.Equals("$GPGSV")) {
                // Strip the checksum
                payload = payload.Substring(0, payload.IndexOf('*'));

                try {
                    var values = payload.Split(',');
                    if (values.Length < 6) {
                        throw new FormatException("Bad GSV sentence structure");
                    }

                    var satellites = new SatellitesInView();

                    //$GPGSV,1,1,13,02,02,213,,03,-3,000,,11,00,121,,14,13,172,05*67
                    //1    = Total number of messages of this type in this cycle
                    //2    = Message number
                    //3    = Total number of SVs in view
                    //4    = SV PRN number
                    //5    = Elevation in degrees, 90 maximum
                    //6    = Azimuth, degrees from true north, 000 to 359
                    //7    = SNR, 00-99 dB (null when not tracking)
                    //8-11 = Information about second SV, same as field 4-7
                    //12-15= Information about third SV, same as field 4-7
                    //16-19= Information about fourth SV, same as field 4-7

                    satellites.MessageCount = int.Parse(values[0]);
                    satellites.MessageNumber = int.Parse(values[1]);
                    satellites.Count = int.Parse(values[2]);
                    satellites.Number = int.Parse(values[3]);
                    satellites.Satellite = new Satellite {
                        AngleOfElvation = String.IsNullOrEmpty(values[4]) ? 0 : int.Parse(values[4]),
                        Azimuth = String.IsNullOrEmpty(values[5]) ? 0 : int.Parse(values[5]),
                        SignalStrength = String.IsNullOrEmpty(values[6]) ? 0 : int.Parse(values[6]),
                    };

                    message = satellites;
                    return true;
                } catch (Exception fe) {
                    NMEA.NotifyError(fe.Message);
                    message = null;
                    return false;
                }
            } else {
                message = null;
                return false;
            }
        }
    }
}
