﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS.MessageCatalog {
    /// <summary>
    /// http://www.hemispheregps.com/gpsreference/GPGLL.htm
    /// </summary>
    internal class GllSentence {

        public static bool TryParse(string identifier, string payload, out IGpsMessage message) {
            if (identifier.Equals("$GPGLL")) {
                // Strip the checksum
                payload = payload.Substring(0, payload.IndexOf('*'));

                try {
                    var values = payload.Split(',');
                    if (values.Length < 6) {
                        throw new FormatException("Bad GLL sentence structure");
                    }

                    var location = new Location();

                    location.Position = NMEA.ParsePosition(String.Concat(values[0], ',', values[1], ';', values[2], ',', values[3]));
                    if (values[4].Length > 5) {
                        int hour = int.Parse(values[4].Substring(0, 2));
                        int minute = int.Parse(values[4].Substring(2, 2));
                        int second = int.Parse(values[4].Substring(4, 2));

                        var utc = DateTime.UtcNow;
                        location.Time = new DateTime(utc.Year, utc.Month, utc.Day, hour, minute, second, DateTimeKind.Utc);
                    } else {
                        throw new FormatException("Invalid time");
                    }
                    location.Status = (values[5].Equals("A") ? Status.Valid : Status.Invalid);

                    message = location;
                    return true;

                } catch (Exception fe) {
                    NMEA.NotifyError(fe.Message);
                    message = null;
                    return false;
                }
            } else {
                message = null;
                return false;
            }
        }
    }
}
