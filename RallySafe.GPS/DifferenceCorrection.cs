﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class DifferenceCorrection {
        /// <summary>
        /// Age, in seconds, of the DGPS correction.
        /// </summary>
        public decimal Age { get; set; }

        /// <summary>
        /// The DGPS station identifier.
        /// </summary>
        public int StationID { get; set; }
    }
}
