﻿using RallySafe.GPS.MessageCatalog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class NMEA {

        public static event Action<string> Error;
        public static event Action<string> Information;

        internal static Direction ParseDirection(string str) {
            if (string.IsNullOrEmpty(str)) {
                return Direction.Empty;
            }

            switch (str.ToUpper()) {
                case "N":
                    return Direction.North;
                case "S":
                    return Direction.South;
                case "E":
                    return Direction.East;
                case "W":
                    return Direction.West;
                default:
                    throw new FormatException("Direction must be N, S, E, or W.");
            }
        }

        internal static Elevation ParseElevation(string str) {
            string[] halves = str.Trim().Split(',');
            if (halves.Length < 2) {
                throw new FormatException("Input string must be in the format 2398,M");
            }

            return new Elevation {
                Value = decimal.Parse(halves[0]),
                UnitType = ParseUnitType(halves[0])
            };
        }

        internal static UnitType ParseUnitType(string str) {
            if (string.IsNullOrEmpty(str)) {
                return UnitType.Unknown;
            }

            switch (str.ToUpper()) {
                case "M":
                    return UnitType.Meters;
                default:
                    return UnitType.Unknown;
            }
        }

        internal static FixMode ParseFixMode(string str) {
            if (string.IsNullOrEmpty(str)) {
                return FixMode.Unknown;
            }

            switch (str.ToUpper()) {
                case "M":
                    return FixMode.Manual;
                case "A":
                    return FixMode.Automatic;
                default:
                    return FixMode.Unknown;
            }
        }

        internal static GpsMode ParseGpsMode(string str) {
            if (string.IsNullOrEmpty(str)) {
                return GpsMode.Unknown;
            }

            switch (str.ToUpper()) {
                case "A":
                    return GpsMode.Autonomous;
                case "D":
                    return GpsMode.Differential;
                case "E":
                    return GpsMode.DeadReckoning;
                case "M":
                    return GpsMode.ManualInput;
                case "S":
                    return GpsMode.Simulator;
                default:
                    return GpsMode.Unknown;
            }
        }

        internal static GeoidalSeparation ParseGeoidalSeparation(string str) {
            string[] halves = str.Trim().Split(',');
            if (halves.Length < 2) {
                throw new FormatException("Input string must be in the format -3569.2,M");
            }

            return new GeoidalSeparation {
                Difference = decimal.Parse(halves[0]),
                UnitType = ParseUnitType(halves[1])
            };
        }

        internal static MagneticVariation ParseMagneticVariation(string str) {
            string[] halves = str.Split(',');
            if (halves.Length < 2) {
                throw new FormatException("Input string must be in the format of 33.02,E");
            }

            decimal degrees = 0.0M;
            if (!string.IsNullOrEmpty(halves[0])) {
                degrees = decimal.Parse(halves[0]);
            }

            return new MagneticVariation {
                Degrees = degrees,
                Direction = ParseDirection(halves[1])
            };
        }

        internal static PositionalDegrees ParsePositionalDegrees(string str) {
            string[] halves = str.Trim().Split(',');
            if (halves.Length < 2) {
                throw new FormatException("Input string must be in the format 23490.32,N");
            }

            var degrees = Double.Parse(halves[0]);
            var degs = Math.Floor(degrees / 100D);
            var mins = ((degrees - (degs * 100D)) / 60D);
            var decimalDegrees = degs + mins;

            return new PositionalDegrees {
                Degrees = decimalDegrees,
                Direction = ParseDirection(halves[1])
            };
        }

        internal static Position ParsePosition(string str) {
            string[] halves = str.Split(';');
            if (halves.Length < 2) {
                throw new FormatException();
            }

            var latitude = ParsePositionalDegrees(halves[0]);
            var longitude = ParsePositionalDegrees(halves[1]);
            return new Position {
                Latitude = latitude.Degrees * ((latitude.Direction == Direction.North) ? 1D : -1D),
                Longitude = longitude.Degrees * ((longitude.Direction == Direction.East) ? 1D : -1D)
            };
        }

        public static IGpsMessage ParseMessage(string str) {
            var identifier = str.Trim().Substring(0, 6).ToUpper();
            var payload = str.Substring(7);  // Also trim trailing comma after identifier

            IGpsMessage message = null;
            if (GgaSentence.TryParse(identifier, payload, out message)) {
                return message;
            } else if (GsaSentence.TryParse(identifier, payload, out message)) {
                return message;
            } else if (GsvSentence.TryParse(identifier, payload, out message)) {
                return message;
            } else if (RmcSentence.TryParse(identifier, payload, out message)) {
                return message;
            } else if (VtgSentence.TryParse(identifier, payload, out message)) {
                return message;
            } else if (GllSentence.TryParse(identifier, payload, out message)) {
                return message;
            } else {
                NotifyError($"Unknown message: {str}");
                return null;
            }
        }

        internal static void NotifyError(string str) {
            Error?.Invoke(str);
        }

        internal static void NotifyInformation(string str) {
            Information?.Invoke(str);
        }
    }
}
