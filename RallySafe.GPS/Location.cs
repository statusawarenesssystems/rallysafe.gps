﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {
    public class Location : IGpsMessage {

        public DateTime Time { get; set; }
        public Position Position { get; set; }
        public Status Status { get; set; }

    }
}
