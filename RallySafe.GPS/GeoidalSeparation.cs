﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallySafe.GPS {

    public class GeoidalSeparation {
        public decimal Difference { get; set; }
        public UnitType UnitType { get; set; }
    }
}
