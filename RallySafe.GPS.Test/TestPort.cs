﻿using Hardware.ComPort.Experimental;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace RallySafe.GPS.Test {
    class TestPort : IPort {
        public ITargetBlock<byte[]> WriteBlock => throw new NotImplementedException();

        public ISourceBlock<byte[]> ReadBlock => bufferBlock;

        public string Name => throw new NotImplementedException();

        public event Action Closed;
        public event Action<string> ErrorReceived;

        public void Close() {
            throw new NotImplementedException();
        }

        private BufferBlock<byte[]> bufferBlock = new BufferBlock<byte[]>();

        public void AddTestData(byte[] data) => bufferBlock.Post(data);
    }
}
