using Hardware.ComPort.Experimental;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RallySafe.GPS.Receiver;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RallySafe.GPS.Test {
    [TestClass]
    public class GpsInterfaceTests {

        private IGpsInterface gpsInterface;
        private TestPort port;
        private SemaphoreSlim slim;

        public GpsInterfaceTests() {

            port = new TestPort();
            gpsInterface = new GpsInterface(port);
            slim = new SemaphoreSlim(0);
        }

        [TestMethod]
        public async Task TestDecodeGpgll() {

            var b = Encoding.UTF8.GetBytes(Environment.NewLine);

            // SETUP
            var testString = "$GPGLL,4232.55945,N,00928.84199,E,143723.00,A,A*68\r\n";
            var testBytes = Encoding.UTF8.GetBytes(testString);

            gpsInterface.MessageReceived += msg => {
                if (msg is Location) {
                    slim.Release();
                }
            };

            // ACT
            port.AddTestData(testBytes);

            // ASSERT
            bool wasFired = await WaitForSlimAsync(1000);

            Assert.IsTrue(wasFired);
        }

        [TestMethod]
        public async Task TestDecodeGpgga() {

            // SETUP
            var testString = "$GPGGA,143723.00,4232.55945,N,00928.84199,E,1,09,0.93,13.9,M,46.7,M,,*60\r\n";
            var testBytes = Encoding.UTF8.GetBytes(testString);

            gpsInterface.MessageReceived += msg => {
                if (msg is Fix) {
                    slim.Release();
                }
            };

            // ACT
            port.AddTestData(testBytes);

            // ASSERT
            bool wasFired = await WaitForSlimAsync(1000);

            Assert.IsTrue(wasFired);
        }

        [TestMethod]
        public async Task TestDecodeTwoMessages() {

            // SETUP
            var testString = "$GPGLL,4232.55945,N,00928.84199,E,143723.00,A,A*68\r\n$GPGGA,143723.00,4232.55945,N,00928.84199,E,1,09,0.93,13.9,M,46.7,M,,*60\r\n";
            var testBytes = Encoding.UTF8.GetBytes(testString);

            int messagesReceived = 0;

            gpsInterface.MessageReceived += msg => {
                Interlocked.Increment(ref messagesReceived);
                if(messagesReceived == 2) {
                    slim.Release();
                }
            };

            // ACT
            port.AddTestData(testBytes);

            // ASSERT
            bool wasFired = await WaitForSlimAsync(1000);

            Assert.IsTrue(wasFired);
        }

        [TestMethod]
        public async Task TestDecodeOneAndAHalfMessages() {

            // SETUP
            var testString = "$GPGLL,4232.55945,N,00928.84199,E,143723.00,A,A*68\r\n$GPGLL,4232.55671,N,";
            var testBytes = Encoding.UTF8.GetBytes(testString);

            var testString2 = "00928.84197,E,090313.00,A,A*66\r\n";
            var testBytes2 = Encoding.UTF8.GetBytes(testString2);


            int messagesReceived = 0;

            gpsInterface.MessageReceived += msg => {
                Interlocked.Increment(ref messagesReceived);
                if (messagesReceived == 2) {
                    slim.Release();
                }
            };

            // ACT
            port.AddTestData(testBytes);
            port.AddTestData(testBytes2);

            // ASSERT
            bool wasFired = await WaitForSlimAsync(1000);

            Assert.IsTrue(wasFired);
        }

        [TestMethod]
        public void TestDecodeInvalidMessageDoesNotThrowException() {
            // SETUP
            var testString = "99,E,143723.00,A,A*68\r\n";
            var testBytes = Encoding.UTF8.GetBytes(testString);

            // ACT
            port.AddTestData(testBytes);

            // ASSERT
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TestDecodeThreePartMessage() {
            // SETUP
            var testString = "$GPGLL,4232.55945,N";
            var testBytes = Encoding.UTF8.GetBytes(testString);

            var testString2 = ",00928.84199,E,143";
            var testBytes2 = Encoding.UTF8.GetBytes(testString2);

            var testString3 = "723.00,A,A*68\r\n";
            var testBytes3 = Encoding.UTF8.GetBytes(testString3);

            gpsInterface.MessageReceived += msg => {
                if (msg is Location) {
                    slim.Release();
                }
            };

            // ACT
            port.AddTestData(testBytes);
            port.AddTestData(testBytes2);
            port.AddTestData(testBytes3);

            // ASSERT
            bool wasFired = await WaitForSlimAsync(1000);

            Assert.IsTrue(wasFired);
        }

        [TestMethod]
        public async Task TestDecodeManyRawLines() {
            // SETUP
            var testString = "$GPTXT,01,01,02,u-blox ag - www.u-blox.com*50\r\n$GPTXT,01,01,02,HW UBX-G60xx  00040007 FFF1FFFFp * 55\r\n$GPTXT,01,01,02,ROM CORE 7.03(45969) Mar 17 2011 16:18:34 * 59\r\n$GPTXT,01,01,02,ANTSUPERV = AC SD PDoS SR * 20\r\n$GPTXT,01,01,02,ANTSTATUS = OK * 3B\r\n$GPRMC,183205.00,A,4232.55773,N,00928.84443,E,0.033,,260319,,,A * 7A\r\n$GPVTG,,T,,M,0.033,N,0.061,K,A * 24\r\n$GPGGA,183205.00,4232.55773,N,00928.84443,E,1,08,1.07,19.6,M,46.7,M,,*6C\r\n$GPGSA,A,3,26,21,16,20,27,10,29,31,,,,,1.81,1.07,1.47 * 01\r\n$GPGSV,3,1,09,08,01,279,,10,20,163,24,16,55,307,36,20,40,142,23 * 70\r\n$GPGSV,3,2,09,21,68,053,39,26,77,236,35,27,30,288,19,29,14,085,43 * 7D\r\n$GPGSV,3,3,09,31,14,205,28 * 4A\r\n$GPGLL,4232.55773,N,00928.84443,E,183205.00,A,A * 6C\r\n$GPRMC,183206.00,A,4232.55773,N,00928.84442,E,0.062,,260319,,,A * 7C\r\n$GPVTG,,T,,M,0.062,N,0.115,K,A * 22\r\n$GPGGA,183206.00,4232.55773,N,00928.84442,E,1,08,1.07,19.6,M,46.7,M,,*6E\r\n$GPGSA,A,3,26,21,16,20,27,10,29,31,,,,,1.81,1.07,1.47 * 01\r\n$GPGSV,3,1,09,08,01,279,,10,20,163,23,16,55,307,37,20,40,142,24 * 71\r\n$GPGSV,3,2,09,21,68,053,40,26,77,236,35,27,30,288,18,29,14,085,44 * 75\r\n$GPGSV,3,3,09,31,14,205,28 * 4A\r\n$GPGLL,4232.55773,N,00928.84442,E,183206.00,A,A * 6E\r\n$GPRMC,183207.00,A,4232.55770,N,00928.84443,E,0.040,,260319,,,A * 7F\r\n$GPVTG,,T,,M,0.040,N,0.074,K,A * 24";
            var testBytes = Encoding.UTF8.GetBytes(testString);

            gpsInterface.MessageReceived += msg => {
                if (msg is Location) {
                    slim.Release();
                }
            };

            // ACT
            port.AddTestData(testBytes);

            // ASSERT
            bool wasFired = await WaitForSlimAsync(1000);

            Assert.IsTrue(wasFired);
        }

        private async Task<bool> WaitForSlimAsync(int timeoutMs) {
            var task = slim.WaitAsync();
            if (await Task.WhenAny(task, Task.Delay(timeoutMs)) == task) {
                // Task completed within timeout.
                // Consider that the task may have faulted or been canceled.
                // We re-await the task so that any exceptions/cancellation is rethrown.
                await task;
                return true;

            } else {
                // timeout/cancellation logic
                return false;
            }
        }
    }
}
